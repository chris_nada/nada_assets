#pragma once

#include <string>
#include <memory>

namespace libzippp { class ZipArchive; }

/**
 *
 */
class nada_assets final {

public:

    /**
     *
     *
     * @param path Absolute or relative path to root directory that contains assets.
     * @param extensions Leave empty for all.
     */
    static void pack_path(const char* path, const char* root, const char* output = "assets.pack", const char* extensions = "");

    enum class ERROR {
        NO_ERROR,
        ENTRY_NOT_FOUND,
        ENTRY_CORRUPT
    };

public:

    /**
     * Constructor.
     * @param asset_file_path Absolute or relative path to the assets file to handle.
     */
    explicit nada_assets(const std::string& asset_file_path);

    /**
     *
     */
    ~nada_assets();

    /**
     *
     * @param asset_name
     * @param case_sensitive
     * @return
     */
    [[nodiscard]] std::string get_asset_str(const std::string& asset_name, bool case_sensitive = true) const;

    /**
     *
     * @return
     */
    [[nodiscard]] bool good() const { return assets_open; }

    /**
     *
     * @return
     */
    [[nodiscard]] ERROR get_last_error() const { return last_error; }

private:

    std::string asset_file_path;

    std::unique_ptr<libzippp::ZipArchive> assets_file;

    bool assets_open;

    mutable ERROR last_error;

};
