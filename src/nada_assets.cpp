#include "../include/nada_assets.hpp"

#include <filesystem>
#include <iostream>
#include <libzippp.h>
#include <fstream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

void nada_assets::pack_path(const char* path, const char* root, const char* output, const char* extensions) {
    std::cout << __func__ << ": " << path << std::endl;

    /// Zip-Archive
    libzippp::ZipArchive za(output);

    /// Cache
    static std::unordered_map<std::string, std::string> path_data;

    // Alle Dateien in Pfad rekursiv durchgehen
    for (const auto& entry : std::filesystem::recursive_directory_iterator(path)) {
        if (!entry.is_regular_file()) continue; // skip other stuff like folders
        const std::string path_abs(entry.path());
        const std::string path_relative(path_abs.begin() + path_abs.find(root), path_abs.end());
        const auto& path = entry.path();
        std::cout << path_relative << ' ';

        // Datei lesen
        if (std::ifstream in(path, std::ios::in | std::ios::binary); in.good()) {
            // Komprimieren nach data
            std::stringstream compressed;
            boost::iostreams::filtering_streambuf<boost::iostreams::input> input_stream;
            boost::iostreams::gzip_params gzip_options;
            gzip_options.level = boost::iostreams::gzip::best_compression;
            input_stream.push(boost::iostreams::gzip_compressor(gzip_options));
            input_stream.push(in);
            boost::iostreams::copy(input_stream, compressed);
            path_data[path_relative] = compressed.str();
        }
    }
    // Cache schreiben
    za.open(libzippp::ZipArchive::New);
    for (const auto& entry : path_data) za.addData(entry.first, entry.second.data(), entry.second.size());
    za.close();
}

nada_assets::nada_assets(const std::string& asset_file_path) :
        asset_file_path(asset_file_path),
        assets_file(new libzippp::ZipArchive(asset_file_path)),
        last_error(ERROR::NO_ERROR)
{
    // Open archive
    assets_file->open(libzippp::ZipArchive::OpenMode::ReadOnly);
    assets_open = assets_file->isOpen();
    std::cout << assets_file->getNbEntries() << " entries loaded\n";
}

nada_assets::~nada_assets() {
    if (assets_file && assets_file->isOpen()) assets_file->close();
}

std::string nada_assets::get_asset_str(const std::string& asset_name, bool case_sensitive) const {
    // Read entry
    const auto& entry = assets_file->getEntry(asset_name, false, case_sensitive);
    if (entry.isNull()) { last_error = ERROR::ENTRY_NOT_FOUND; return ""; }
    std::stringstream compressed;
    if (entry.readContent(compressed) !=  LIBZIPPP_OK) { last_error = ERROR::ENTRY_CORRUPT; return ""; }

    // Decompress and return
    try {
        std::stringstream decompressed;
        boost::iostreams::filtering_streambuf<boost::iostreams::input> out;
        out.push(boost::iostreams::gzip_decompressor());
        out.push(compressed);
        boost::iostreams::copy(out, decompressed);
        last_error = ERROR::NO_ERROR;
        return decompressed.str();
    } catch (...) {
        last_error = ERROR::ENTRY_CORRUPT;
        return "";
    }
}
