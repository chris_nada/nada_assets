#include "include/nada_assets.hpp"

#include <vector>
#include <iostream>

int main(int argc, char** argv) {
    std::vector<const char*> params;
    for (int i = 1; i < argc; i++) params.emplace_back(argv[i]);
    if      (params.size() == 3) nada_assets::pack_path(params[0], params[1], params[2]);
    else if (params.size() == 2) nada_assets::pack_path(params[0], params[1]);
    else {
        std::cerr << "Wrong number of arguments passed: Need at least [path] to root of assets (relative or absolute)\n"
                     "and name of the [root] directory.\n"
                     "For example: " << argv[0] << " /home/documents/my_game/assets/ my_game\n";
        return 1;
    }
}
