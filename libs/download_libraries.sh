#!/bin/sh

# Automatisch Library hinzufügen
lib_download () {
  if [ ! -d "$1" ]; then                 # Nur wenn noch nicht existiert
    wget --output-document="$1".zip "$3" # Herunterladen
      unzip "$1".zip                     # Entpacken
      rm "$1".zip                        # zip löschen
      mv "$1"-"$2" "$1"                  # Version aus Ordnernamen entfernen
  fi
}

#           | Library, Version, Download-Link
lib_download libzip 1.8.0 https://github.com/nih-at/libzip/archive/refs/tags/v1.8.0.zip
lib_download libzippp-libzippp v5.0-1.8.0 https://github.com/ctabin/libzippp/archive/refs/tags/libzippp-v5.0-1.8.0.zip
mv libzippp-libzippp libzippp